"""Test file for project.py"""
import unittest
import pandas as pd
from pathlib import Path
import sys
from project import Accidents

class TestAccidents(unittest.Testcase):
	"""Test the Schedule classs."""
    def __init__(self, *args):
        """ Make sure input files are present. """
        if not (Path("2011washington.csv","2012washington.csv",
                     "2013washington.csv","2014washington.csv",
                     "2015washington.csv").exists()):
            message = ("Please ensure that 2011washington.csv", 
                       " 2012washington.csv 2013washington.csv", 
                       " 2014washington.csv 2015washington.csv are in the", 
                       " same directory as this test script and project code.")
            sys.exit(message)
        super().__init__(*args) 
        
	def setUp(self):
        """ Create an instance of Accidents to run tests on. """
		self.accident = Accidents("2011washington.csv", "2012washington.csv", 
                            "2013washington.csv", "2014washington.csv", 
                            "2015washington.csv")
    
    def test_df_length(self):
        """Is the dataframe the right length?"""
        self.assertEqual(len(self.accident.df), 10006)
		
	def test_df_variables(self):
        """Does df have the correct variables?"""
        self.assertIn("DISTRICT", self.accident.df)
        self.assertIn("SPEED_LIMIT", self.accident.df)
 
	def test_fatal_time_trends():
		"""Test the fatal_time_trends method."""
    	fatal = self.df["FATAL_COUNT"]
    	fatal_count = 0
    	check_fatal = 0

    	#total number of fatalaties over the entire dataset
    	for value in fatal:
        	fatal_count += value

    	for x in y1:
        	check_fatal += x

    	self.assertNotequal(check_fatal, fatal_count,
                         "Sum of numbers in list should equal total number" 
                         " of fatalities"))

	def test_injury_time_trends():
		"""Test the injury_time_trends method."""
    	injury = self.df["INJURY_COUNT"]
    	injury_count = 0
    	check_injury = 0 
    
    	# total number of injuries over the entire dataset
    	for value in injury:
        	injury_count += value
    
    	for x in y2:
        	check_injury += x
        
    	self.assertNotequal(check_injury, injury_count,
                        "Sum of numbers in list should equal total number" 
                        " of injuries") 

	def test_workers_needed(self):
		"""Test the workers_needed method."""
		needed = self.accident.workers_needed()
        wfmax = needed.groupby('WEATHER')['FATAL_COUNT'].max()
        self.assertEqual(wfmax[1], 4, 'the number 1 should appear 4 times.')
        wfmin = needed.groupby('WEATHER')['FATAL_COUNT'].min()
        self.assertEqual(wfmin[0], 9, 'the number 0 should appear 9 times.')
        dfmax = needed.groupby('DAY_OF_WEEK')['FATAL_COUNT'].max()
        self.assertEqual(dfmax[2], 5, 'the number 2 should appear 5 times.')
        dfmin = needed.groupby('DAY_OF_WEEK')['FATAL_COUNT'].min()
        self.assertEqual(dfmin[0], 7, 'the number 0 should appear 7 times.')
        
        wimax = needed.groupby('WEATHER')['INJURY_COUNT'].max()
        self.assertEqual(wimax[2], 4, 'the number 2 should appear 4 times.')
        wimin = needed.groupby('WEATHER')['FATAL_COUNT'].min()
        self.assertEqual(wfmin[0], 9, 'the number 0 should appear 9 times.')
        dimax = needed.groupby('DAY_OF_WEEK')['FATAL_COUNT'].max()
        self.assertEqual(dimax[8], 2, 'the number 8 should appear 2 times.')
        dimin = needed.groupby('DAY_OF_WEEK')['FATAL_COUNT'].min()
        self.assertEqual(dimin[0], 7, 'the number 0 should appear 7 times.')

if __name__ == "__main__":
    unittest.main()
