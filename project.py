import pandas as pd
import sys
from argparse import ArgumentParser
import matplotlib.pyplot as plt
class Accidents:
    """Calculates how many workers are needed based on given factors.
    
    Attributes:
        data (list): list of washington files
        df (path): path containing all csv files
        stuff (list): a list of values
    """
    def __init__(self, files):
        """Initializes list of files.

        Args:
            files (path): containing all the washington files
        """
        self.files = files
        self.data = [pd.read_csv(file) for file in self.files]
        self.df = pd.concat(self.data)
        self.stuff = self.df.values.tolist()
        
    def correlation(self, condition1, condition2):
        """Displays correlation graph of two conditions that may cause 
        accidents.
        
        Args: 
            condition1 (string): the first condition from dataset
            condition2 (string): the second conitdion from dataset
        """
       
        # Create a scatter plot based on conditions that the user chose
        graph = self.df.plot.scatter(x = condition1, y = condition2)
        plt.xlabel(condition1)
        plt.ylabel(condition2)
        plt.show()
        
    def fatal_time_trends(self):
        """Displays a line graph of number fatalities based on the day of the 
        week."""
        fatals = {1:0,2:0,3:0,4:0,5:0,6:0,7:0}

        # Creates dictionaries for the total number of fatalaties per day 
        for line in self.stuff:
            day = line[7]
            #fatal count variable
            f = line[32]
            
            if day in fatals:
                fatals[day] += f
       
        x1 = []
        y1 = []
        for key in fatals:
            day = key
            x1.append(day)
            fatal = fatals[key]
            y1.append(fatal)

        fatals_df = pd.DataFrame(y1, index = x1, columns = ["FATAL_COUNT"])

        fatals_df.plot()
        plt.xlabel("Day of the Week")
        plt.ylabel("Number of Fatalities")
        plt.show()
        
    def injury_time_trends(self):
        """Displays a line graph of number injuries based on the day of the 
        week."""
        injury = {1:0,2:0,3:0,4:0,5:0,6:0,7:0}

        for line in self.stuff:
            day = line[7]
            #injury count variable
            i = line[33]
        
            if day in injury:
                injury[day] += i 
        
        x2 = []
        y2 = []
        for key in injury:
            day = key
            x2.append(day)
            i = injury[key]
            y2.append(i)
    
        injury_df = pd.DataFrame(y2, index = x2, columns = ["INJURY_COUNT"])

        injury_df.plot()
        plt.xlabel("Day of the Week")
        plt.ylabel("Number of Injuries")
        plt.show()

    def workers_needed(self):
        """Returns number of workers needed based on weather and day.
            
        Return:
            Returns a string depending on the user's input
        """
        weather_max = {}
        weather_min = {}
        weather_mean = {}
        day_max = {}
        day_min = {}
        day_mean = {}

        while True:
            try:
                weather = float(input('Enter a number between 0 - 30: '))
                day = float(input('Enter a number between 0 - 30: '))

            except ValueError:
                print('You entered an invalid number. Try again.')

            else:
                if (weather > 30 or weather < 0):
                    print('Please try again.')

                elif (day > 30 or day < 0):
                    print('Please try again.')

                else:
                    break

        fatal_max = self.df.groupby('WEATHER')['FATAL_COUNT'].max()
        for key, value in fatal_max.items():
            weather_max[key] = value
        injury_max = self.df.groupby('WEATHER')['INJURY_COUNT'].max()
        for key, value in injury_max.items():
            weather_max[key] += value

        fatal_min = self.df.groupby('WEATHER')['FATAL_COUNT'].min()
        for key, value in fatal_min.items():
            weather_min[key] = value
        injury_min = self.df.groupby('WEATHER')['INJURY_COUNT'].min()
        for key, value in injury_min.items():
            weather_min[key] += value

        fatal_mean = self.df.groupby('WEATHER')['FATAL_COUNT'].mean()
        for key, value in fatal_mean.items():
            weather_mean[key] = value
        injury_mean = self.df.groupby('WEATHER')['INJURY_COUNT'].mean()
        for key, value in injury_mean.items():
            weather_mean[key] += value

        fatal_day_max = self.df.groupby('DAY_OF_WEEK')['FATAL_COUNT'].max()
        for key, value in fatal_day_max.items():
            day_max[key] = value
        injury_day_max = self.df.groupby('DAY_OF_WEEK')['INJURY_COUNT'].max()
        for key, value in injury_day_max.items():
            day_max[key] += value

        fatal_day_min = self.df.groupby('DAY_OF_WEEK')['FATAL_COUNT'].min()
        for key, value in fatal_day_min.items():
            day_min[key] = value
        injury_day_min = self.df.groupby('DAY_OF_WEEK')['INJURY_COUNT'].min()
        for key, value in injury_day_min.items():
            day_min[key] += value

        fatal_day_mean = self.df.groupby('DAY_OF_WEEK')['FATAL_COUNT'].mean()
        for key, value in fatal_day_mean.items():
            day_mean[key] = value
        injury_day_mean = self.df.groupby('DAY_OF_WEEK')['INJURY_COUNT'].mean()
        for key, value in injury_day_mean.items():
            day_mean[key] += value

        above_w = max(weather_max.values())
        average_w = weather_mean[8]
        below_w = min(weather_min.values())

        above_d = max(day_max.values())
        average_d = day_mean[1]
        below_d = min(day_min.values())

        if ((weather >= below_w and weather < average_w) and
            (day >= below_d and day < average_d)):
            return('Less than the average amount of workers. Schedule 10'
                ' workers')

        elif ((weather > average_w and weather <= above_w) and
              (day > average_d and day <= above_d)):
            return('Greater than the average amount of workers. Schedule 30'
                ' workers')

        else:
            return('Average amount of workers. Schedule 15 workers')

def parse_args(arglist):
    """Parse for all csv files.
    
    Args:
        arglist (path): all csv files
        
    Return:
        args (path): the csv files
    """
    parser = ArgumentParser()
    parser.add_argument("washington_files", nargs="+", 
                        help="CSV files containing" "washington accidents")
    args = parser.parse_args(arglist)
    return args

def main(arglist):
    """This function will call the Schedule class.
    
    Args:
        arglist (path): all csv files
    
    Side Effect:
        print out workers_needed method.
    """
    args = parse_args(arglist)
    accident = Accidents(args.washington_files)
    user = input("Do you want to see correlation between events,"
        " fatal time trends, injury time trends, or create a schedule? ")
    print(user)
    if user == 'correlation':
        lst1 = ["FATAL_COUNT", "INJURY_COUNT", "UNBELTED_DEATH_COUNT", 
                "BELTED_DEATH_COUNT"]
        lst2 = ["DAY_OF_WEEK", "TIME_OF_DAY" "ILLUMINATION", "WEATHER", 
                "ROAD_CONDITION", "INTERSECT_TYPE"]
        print(lst1, lst2)
        cond1 = input('Please enter your first condition? ')
        cond2 = input('Please enter your second condition? ')
        print(accident.correlation(cond1, cond2))
    elif user == 'fatal time trends':
        print(accident.fatal_time_trends())
    elif user == 'injury time trends':
        print(accident.injury_time_trends())
    elif user == 'schedule':
        print(accident.workers_needed())
    else:
        print('You entered an incorrect string. Please try again.')

if __name__ == "__main__":
    main(sys.argv[1:])
